<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- ↓Springフレームワークが提供してくれているtagを使うための宣言 -->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>
        <h2>${message}</h2>

        <!-- form:fromのmodelAttributeに指定する値と、実際にControllerでバインドさせるFormオブジェクトは名前を一致させる -->
        <form:form modelAttribute="testForm">
			<!-- pathに指定する値はFormオブジェクトのプロパティ名/フィールド名と合わせる -->
            <form:input path="id" />
            <form:input path="name" />
            <input type="submit">
        </form:form>
    </body>
</html>