<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>${message}</p>

	<form:form modelAttribute="checkForm"
		action="${pageContext.request.contextPath}/sample/check/info/">
		<form:checkboxes path="employees" items="${checkEmployees}"
			delimiter="/" /> <!-- delimiterは単純に複数要素を表示する際の表示分けに、どんな文字列を使用するかを定義してる -->
		<input type="submit" />
	</form:form>
</body>
</html>