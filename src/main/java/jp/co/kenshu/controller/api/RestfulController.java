package jp.co.kenshu.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.service.TestService;

@RestController
public class RestfulController {

	// テキストが返る※@RestControllerが付いているので、@ResponseBodyが不要
	@RequestMapping(value = "/api/text/")
	public String getTestMember() {
		return "ALOHA";
	}

	// XMLをレスポンスとして返却する例  ☆↓produceでメディアタイプを指定
	@RequestMapping(value = "/xml/", produces = "application/xml")
	public String xml1() {

		//戻り値がそもそもXML形式の文字列である必要あり
		return "<a><b>content</b></a>";
	}

	@Autowired
	private TestService service;

	// JSON返却する例※jackson.coreとjackson-databindがPOMに必要
	@RequestMapping("/api/json/")
	public List<TestDto> json() {
	    List<TestDto> tests = service.getTestAll();
	    return tests;
	}
}