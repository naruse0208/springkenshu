package jp.co.kenshu.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import jp.co.kenshu.validator.MemoValidator;

//JavaDocによってドキュメント化される
@Documented
//Validatorを行うクラスを指定
@Constraint(validatedBy = MemoValidator.class)

//今回新規で作成したMemoValidatorを指定
//メソッドとフィールドに対して付与できるアノテーションを表している
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME) //アノテーションの有効範囲を指定
public @interface Memo {
	String message() default "Please input a memo.";

	//↓この追加だけでアノテーションに指定できるオプションが追加される→Formで@Memo(50)と指定OKに！
	int value();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}