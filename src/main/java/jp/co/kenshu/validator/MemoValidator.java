package jp.co.kenshu.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jp.co.kenshu.validator.annotation.Memo;

//実際に入力値を検証するクラス
//↓「String型の文字列をMemo型を用いてValidatorします」という意味
public class MemoValidator implements ConstraintValidator<Memo, String> {

	//このクラスのフィールドとして「maxValue」を宣言
	private int maxValue;

	@Override
	public void initialize(Memo memo) {
		//アノテーションに指定された値を受け取るために代入
		//memoが使えるのは、implements ConstraintValidator<Memo, String>でMemo型を約束されているから
		this.maxValue = memo.value();
	}

	@Override
	public boolean isValid(String input, ConstraintValidatorContext con) {
		if (input == null) {
			return false; //validatorにひっかかったことになり、propertiesに記述されたメッセージが呼び出される
		}
		if (input.matches("^[0-9]*$")) {
			return false; //（〃）propertiesに記述されたメッセージが呼び出される
		}
		if (maxValue < input.length()) {
			return false;
		}
		return true;
	}
}