package jp.co.kenshu.dto.employee;

public class EmployeeDto {

	//クライアント入力値→Form
	//ビジネスロジック表示情報→Dto
	//今回はJSPの「not empty」でforeachしているListにこのDtoが詰められている

	private String name;
	private Integer age;
	private String memo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}
