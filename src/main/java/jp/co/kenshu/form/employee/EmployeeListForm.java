package jp.co.kenshu.form.employee;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import jp.co.kenshu.validator.annotation.Memo;

public class EmployeeListForm {

	//クライアント入力値→Form
	//ビジネスロジック表示情報→Dto

	@NotEmpty(message = "名前は必須です") //@NotEmpty：入力値検証アノテーションを追加
	private String name;

	@NotNull //@NotNull：入力値検証アノテーションを追加（Integerには@NotEmptyは使えず、@NotNullを使えとのこと）
    @Min(value = 18, message = "{value}以上の値を設定してください") //@Min：入力値検証アノテーションを追加
	private Integer age;
	/** ageをintではなくIntegerを使用している理由は、
	    jspのpath指定したageがデフォルトで「0」と表示されてしまうのを防ぐため
	 **/

	@Memo(50) //独自Validationを作成(Memo.java)に伴いアノテーションを付与
	private String memo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}
