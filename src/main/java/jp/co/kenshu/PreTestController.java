package jp.co.kenshu;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.TestForm;

@Controller //SpringアプリケーションにこのクラスはControllerってことを教えている
public class PreTestController {

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String showMessage(Model model) {
		TestForm form = new TestForm();
		form.setId(0);
		form.setName("ここに名前を書いてね");
		model.addAttribute("testForm", form); //form:fromのmodelAttributeに指定する値と一致させる
		model.addAttribute("message", "FORMの練習");
		return "showMessage";
	}

	@RequestMapping(value = "/show", method = RequestMethod.POST)
	//↓@ModelAttributeで指定したオブジェクトがjspで指定した
	//<form:form modelAttribute="testForm">と関連し、バインドされる
	public String getFormInfo(@ModelAttribute TestForm form, Model model) {
		model.addAttribute("message", "ID : " + form.getId() + " & name : " + form.getName());
		return "showMessage";
	}

	/**
	//valueに記述した文字列でこのControllerの住所(URL)はここってのを教えている
	//「method = RequestMethod.GET」はクライアントがGETでアクセスしたケースを指している
	//POSTであれば「RequestMethod.POST」となる。SpringFrameworkが提供するAPIを利用している
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String showMessage(Model model) {
		//Modelは、Viewとの橋渡しをするためのクラス
		model.addAttribute("message", "hello world!!");

		//↑「addAttribute」メソッドは、Modelインスタンスに属性を追加するもの
		//第一引数には属性のkey値を、第二引数には値をそれぞれ指定

		return "showMessage";
	}
	**/
}